// ==UserScript==
// @name        br3_etc
// @namespace   http://nyo.sp.land.to/
// @description そのたいろいろツール by んょ
// @include     http://*.3gokushi.jp/item/*
// @resource	COIN_S	http://nyonyo.ddo.jp/saru/labo/coin.ogg
// @resource	ONE_UP_S	http://nyonyo.ddo.jp/saru/labo/1up.ogg
// @version     1.04
// ==/UserScript==
// ver1.00 2012.10.07 んょ 新規作成（受信箱アイテム一括移動）
// ver1.01 2012.10.31 んょ 水鏡先生も移動するよ
// ver1.02 2012.11.09 んょ こいーん
// ver1.03 2013.02.10 んょ 高速化
// ver1.04 2013.11.09 んょ 仕様変更対応

var d=document;
var $ = function(id) { return d.getElementById(id); };
var $x = function(xp,dc) {
	return d.evaluate(xp, dc||d, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
};

var $a = function(xp,dc) {
	var r = d.evaluate(xp, dc||d, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);
	var a=[];
	for(var i=0; i<r.snapshotLength; i++){
		a.push(r.snapshotItem(i));
	}
	return a;
};
var $e = function(key) { return d.evaluate(key, document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null); };
var $l = function(dc,e,f) { if (!dc) return; dc.addEventListener(e, f, false); };

var MAX_COIN_S = 3;

var itemLen = 0;
var coinCount = [0,0,0];
var ssid="";
var cntCoinS=0;

// 受信箱のアイテムを全て移動する
function moveAllItem()
{
	var log=$('move_log');
	var items=$e('//a[@class="item_icon_switch"]');
	var ssid=$x('//input[@name="ssid"]');
	
	$('move_all').disabled="true";
	
	itemLen=items.snapshotLength;
	if(!itemLen) {
		log.innerHTML="移動させるアイテムがありません<br>";
	} else {
	log.innerHTML="---移動開始---<br>";
	for(i=0;i<items.snapshotLength;i++){
		log.innerHTML=items.snapshotItem(i).getAttribute("title")+"<br>"+log.innerHTML;
//		log.innerHTML=items.snapshotItem(i).getAttribute("onclick").split(',')[2]+"<br>"+log.innerHTML;

		var req;
		
		if(items.snapshotItem(i).getAttribute("onclick").match(/iu_card/)){
			// 水鏡先生が移動できないので。
			req="item_id=2"+items.snapshotItem(i).getAttribute("onclick").match(/'\.n(\d+)'/)[1]
					+"&ssid="+ssid.value;
			//console.log(items.snapshotItem(i).getAttribute("onclick").match(/'\.n(\d+)'/)[1]);
		} else {
			req="item_id="+items.snapshotItem(i).getAttribute("onclick").split(',')[2]
					+"&ssid="+ssid.value;
		}
		GM_xmlhttpRequest({
			method:"POST", 
			url:"http://" + location.hostname + "/item/inbox.php",
			headers:{"Content-type":"application/x-www-form-urlencoded"},
			data: req,
			onload:function(x){
				if(--itemLen == 0){
					// 最後
					var log=$('move_log');
					log.innerHTML="---移動完了---<br>"+log.innerHTML;
				}
			}
		});
	}
	}
//	log.innerHTML="---移動完了---<br>"+log.innerHTML;
}




// 受信箱
function item_inbox_f()
{
	var iil=$('itemInventoryLeft');
	if(iil){
		var btn=d.createElement('input');
		with(btn){
			id="move_all";
			type="button";
			value="一括移動";
			addEventListener('click',function(){
				moveAllItem();
			}, false);
		}
		
		iil.appendChild(btn);
		
		var win=d.createElement('div');
		with(win){
			id="move_log";
			with(style){
				backgroundColor = "white";
				position = "absolute";
				border = "outset 2px gray";
				fontSize = "11px";
				overflow = "auto";
				width = "250px";
				height = "100px";
			}
		}
		iil.appendChild(win);
	}
	
}


function handleGetCoin(x)
{
	if(x.status != 200) {
		alert("エラー("+x.status+")");
		return;
	}
	
	var htmldoc = document.createElement("html");
	htmldoc.innerHTML = x.responseText;
	
	var c=[],reC=[];
	c[0] = $x('//a[@title="100BPコイン" and @class="item_icon_switch"]/../span[@class="item_icon_stack"]',htmldoc);
	c[1] = $x('//a[@title="500BPコイン" and @class="item_icon_switch"]/../span[@class="item_icon_stack"]',htmldoc);
	c[2] = $x('//a[@title="1000BPコイン" and @class="item_icon_switch"]/../span[@class="item_icon_stack"]',htmldoc);
	for(i=0;i<3;i++){
		if(c[i]){
			reC[i] = parseInt(c[i].innerHTML);
		} else {
			reC[i] = 0;
		}
	}
	

	// Coin Sound
//	console.log("coin");
	d.getElementById("coin_s1").load();
	d.getElementById("coin_s1").play();
	cntCoinS++;
	if(cntCoinS>=MAX_COIN_S){
		cntCoinS=0;
	}
	if(Math.random() * 10 < 1) {
		d.getElementById("one_up_s").play();
	}
	
//console.log("0:"+reC[0]);
//console.log("1:"+reC[1]);
//console.log("2:"+reC[2]);
	if(reC[0]+reC[1]+reC[2]<=0) {
		setTimeout(function(){
			location.reload();
		},1000);
	}
}

function getAllCoin()
{
//	console.log("start");
	var ssid=$x('//input[@name="ssid"]').value;
	var coinStack = [];
	coinStack[0]=$a('//a[@title="100BPコイン" and @class="item_icon_switch"]/../span[@class="item_icon_stack"]');
	coinStack[1]=$a('//a[@title="500BPコイン" and @class="item_icon_switch"]/../span[@class="item_icon_stack"]');
	coinStack[2]=$a('//a[@title="1000BPコイン" and @class="item_icon_switch"]/../span[@class="item_icon_stack"]');

	cntCoinS=0;
	for(i=0;i<3;i++){
		for(j=0;j<coinStack[i].length;j++) {
			var req="allbp_item_id="+(164+i)+"&allbp_item_num="+parseInt(coinStack[i][j].innerHTML)+"&ssid="+ssid;
//console.log(req);
			GM_xmlhttpRequest({
				method:"POST", 
				url:"http://" + location.hostname + "/item/index.php",
				headers:{"Content-type":"application/x-www-form-urlencoded"},
				data: req,
				onload:function(x){
					handleGetCoin(x);
				}
			});
		}
	}
	
	executeGetCoinReq();
}


// 便利アイテム
function item_index_f()
{
	var iil=$('itemInventoryLeft');
	if(iil){
		var btn=d.createElement('input');
		with(btn){
			id="move_all";
			type="button";
			value="BPコイン一括";
			addEventListener('click',function(){
				getAllCoin();
			}, false);
		}
		
		iil.appendChild(btn);
		
		var ad,src;
		
		for(i=0;i<3;i++){
			ad=d.createElement('audio');
			ad.id="coin_s"+i;
			ad.preload="auto";
			iil.appendChild(ad);
		
			src=d.createElement('source');
			src.src=GM_getResourceURL("COIN_S");
			src.type="audio/ogg";
			ad.appendChild(src);
		}

		ad=d.createElement('audio');
		ad.id="one_up_s";
		ad.preload="auto";
		iil.appendChild(ad);
	
		src=d.createElement('source');
		src.src=GM_getResourceURL("ONE_UP_S");
		src.type="audio/ogg";
		ad.appendChild(src);


		var btn=d.createElement('input');
		with(btn){
			id="sound_test";
			type="button";
			value="テスト！";
			addEventListener('click',function(){
				d.getElementById("coin_s1").play();
			}, false);
		}
		
		iil.appendChild(btn);
	}
}




// 
setTimeout(function() { 

	if(location.pathname == "/item/inbox.php") {
		// 受信箱
		item_inbox_f();
	} else if(location.pathname == "/item/index.php" || location.pathname == "/item/") {
		// 便利アイテム
		item_index_f();
	}
		
	
}, 0);
